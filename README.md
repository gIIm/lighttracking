# lightTracking

---

### ABOUT

openFrameworks app for video light tracking and path tracing

tested on of_v0.10.0 in Visual Studio

### DEPENDENCIES
```
- ofxOpenCv
- ofxGui
```
