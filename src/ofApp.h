#pragma once

#include "ofMain.h"

#include "ofxGui.h"
#include "ofxOpenCv.h"
#include "ofxVectorGraphics.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void exit();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		// Video Player
		void setVideoFrame(int & frame);

		ofVideoPlayer	videoPlayer;

		string videoDate;
		string videoOrientation;

		int videoTotalFrames;

		int frameWidth;
		int frameHeight;

		// OpenCV
		ofxCvColorImage	colorImg;

		ofxCvGrayscaleImage	grayImage;
		ofxCvGrayscaleImage grayBg;
		ofxCvGrayscaleImage grayDiff;

		ofxCvContourFinder	contourFinder;

		// Tracking
		void setThreshold(int & thr);
		void setPathResolution(int & res);

		ofImage cameraTopCalibration;

		ofPoint node;

		ofPath path;

		ofxVectorGraphics output;

		vector <ofPoint> pts;

		bool capture;

		int	threshold;
		int rescaleRes;

		unsigned int counterLimit;

		int const sampleRate = 100;

		// GUI
		ofxPanel gui;
		ofParameter<int> videoPlaybackSlider;
		ofxIntSlider thresholdSlider;
		ofxIntSlider resolutionSlider;

		// JSON
		ofJson json;

};
