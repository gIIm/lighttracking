#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

	ofSetFrameRate(30);
	ofSetVerticalSync(true);

	// Video Player
	videoDate = "20181023";
	videoOrientation = "side";

	videoPlayer.load("video/" + videoDate + "/camera-" + videoOrientation + "-" + videoDate + ".mp4");
	videoPlayer.setLoopState(OF_LOOP_NONE);
	videoPlayer.play();

	videoTotalFrames = videoPlayer.getTotalNumFrames();

	frameWidth = videoPlayer.getWidth();
	frameHeight = videoPlayer.getHeight();

	// OpenCV
	colorImg.allocate(frameWidth, frameHeight);
	grayImage.allocate(frameWidth, frameHeight);
	grayBg.allocate(frameWidth, frameHeight);
	grayDiff.allocate(frameWidth, frameHeight);
	
	cameraTopCalibration.load("img/" + videoDate + "/camera-" + videoOrientation + "-calibration-" + videoDate + ".jpg");

	grayBg = cameraTopCalibration.getPixels();

	// Tracking
	threshold = 60;
	rescaleRes = 10;
	counterLimit = sampleRate;

	node.x = 0;
	node.y = 0;

	path.setStrokeColor(ofColor(63, 66, 255));
	path.setFilled(false);
	path.setStrokeWidth(1);

	capture = false;

	// GUI
	gui.setup();
	gui.setPosition(10, 20);
	gui.setName("light tracking");
	gui.add(videoPlaybackSlider.set("frame", 0, 0, videoTotalFrames - 250));
	gui.add(thresholdSlider.setup("threshold", threshold, 0, 255));
	gui.add(resolutionSlider.setup("resolution", rescaleRes, 1, 100));

	videoPlaybackSlider.addListener(this, &ofApp::setVideoFrame);
	thresholdSlider.addListener(this, &ofApp::setThreshold);
	resolutionSlider.addListener(this, &ofApp::setPathResolution);

}

//--------------------------------------------------------------
void ofApp::update(){

	ofBackground(45, 45, 45);

	// Video Player
	bool bNewFrame = false;

	videoPlayer.update();
	bNewFrame = videoPlayer.isFrameNew();
	
	// OpenCV
	if (bNewFrame) {
		colorImg.setFromPixels(videoPlayer.getPixels());

		grayImage = colorImg;

		// take the abs value of the difference between background and incoming and then threshold:
		grayDiff.absDiff(grayBg, grayImage);
		grayDiff.threshold(threshold);

		// find contours which are between the size of 0 pixels and 1/3 the w*h pixels.
		// also, find holes is set to true so we will get interior contours as well....
		contourFinder.findContours(grayDiff, 0, (frameWidth * frameHeight) / 3, 1, true);	// find holes
	}

}

//--------------------------------------------------------------
void ofApp::draw(){

	// draw the incoming, the grayscale, the bg and the thresholded difference
	ofSetHexColor(0xffffff);
	colorImg.draw(220, 20);
	// grayImage.draw(360, 20);
	// grayBg.draw(20, 280);
	grayDiff.draw(560, 20);

	// then draw the contours:
	ofFill();
	ofSetHexColor(0x3d3d3d);
	ofDrawRectangle(220, 280, frameWidth*2+20, frameHeight*2+20);
	ofSetHexColor(0xffffff);

	// draw each blob individually from the blobs vector,
	// this is how to get access to them:
	for (int i = 0; i < contourFinder.nBlobs; i++) {
		ofPushMatrix();
		ofTranslate(220, 280);
		ofScale(2, 2, 2);
		contourFinder.blobs[i].draw(0, 0);
		ofPopMatrix();

		if (ofGetElapsedTimeMillis() > counterLimit) {
			if (node.x != contourFinder.blobs[0].centroid.x) {
				node.x = (int)contourFinder.blobs[0].centroid.x;

				ofJson pt;
				pt["x"] = node.x;
				pt["y"] = node.y;
				json.push_back(pt);

				cout << "x: " << node.x << endl;
			}

			if (node.y != contourFinder.blobs[0].centroid.y) {
				node.y = (int)contourFinder.blobs[0].centroid.y;

				ofJson pt;
				pt["x"] = node.x;
				pt["y"] = node.y;
				json.push_back(pt);

				cout << "y: " << node.y << endl;
			}

			counterLimit += sampleRate;
		}

	}

	// PATH
	ofPushMatrix();
	ofTranslate(220, 280);
	ofScale(2, 2, 2);

	path.lineTo(node.x, node.y);
	path.draw();

	//we add a new point to our line
	pts.push_back(ofPoint());
	int last = pts.size() - 1;

	pts[last].x = node.x;
	pts[last].y = node.y;


	if (capture) {
		output.beginEPS("svg/" + videoDate + "/path-" + videoOrientation + "-" + videoDate + ".ps");
	}
	
	if (pts.size() > 0) {

		int numPts = pts.size();

		output.setColor(0x0088EE);
		output.noFill();
		output.beginShape();

		//catmull curves create nice smooth curves between points
		//so actually a lower resolution creates a smoother line
		//and reduce the number of points in our eps file.
		// int rescaleRes = 60;

		for (int i = 0; i < numPts; i++) {

			//we need to draw the first and last point
			//twice for a catmull curve
			if (i == 0 || i == numPts - 1) {
				output.curveVertex(pts[i].x, pts[i].y);
			}

			if (i % rescaleRes == 0) output.curveVertex(pts[i].x, pts[i].y);
		}

		output.endShape();
	}

	if (capture) {
		output.endEPS();
		capture = false;
	}

	ofPopMatrix();

	// GUI
	gui.draw();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

	switch (key) {
		case 'c':
			path.clear();
			pts.clear();
			break;
		case 's':
			ofSaveJson("json/coords-" + videoDate + ".json", json);
			capture = true;
			break;
		default:
			break;
	}

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//--------------------------------------------------------------
void ofApp::setVideoFrame(int &frame) {

	videoPlayer.setFrame(frame);

}

//--------------------------------------------------------------
void ofApp::setThreshold(int &thr) {

	threshold = thr;

}

//--------------------------------------------------------------
void ofApp::setPathResolution(int &res) {

	rescaleRes = res;

}


//--------------------------------------------------------------
void ofApp::exit() {

	videoPlaybackSlider.removeListener(this, &ofApp::setVideoFrame);

}